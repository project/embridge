<?php

/**
 * @file
 * Hooks for embridge module.
 */

/**
 * Allows modules to operate after selection of an entermedia asset from search.
 *
 * @param object $entity
 *   The entity object that the asset was added to.
 * @param string $entity_type
 *   The type of entity; for example, 'node' or 'user'.
 * @param object $asset
 *   The asset object that was added to the entity.
 */
function hook_embridge_asset_select($entity, $entity_type, $asset) {
  // Make sure the entity is an existing node.
  if ($entity_type == 'node' && isset($entity->nid) && !empty($entity->nid)) {
    db_insert('mytable')
    ->fields(array(
      'aid' => $asset->aid,
      'nid' => $entity->nid,
      'extra' => $asset->extra,
    ))
    ->execute();
  }
}

/**
 * Allows modules to operate on uploading an entermedia asset.
 *
 * @param object $entity
 *   The entity object that the asset was added to.
 * @param string $entity_type
 *   The type of entity; for example, 'node' or 'user'.
 * @param object $asset
 *   The asset object that was added to the entity.
 */
function hook_embridge_asset_upload($entity, $entity_type, $asset) {
  // Make sure the entity is an existing node.
  if ($entity_type == 'node' && isset($entity->nid) && !empty($entity->nid)) {
    db_insert('mytable')
    ->fields(array(
      'aid' => $asset->aid,
      'nid' => $entity->nid,
      'extra' => $asset->extra,
    ))
    ->execute();
  }
}
