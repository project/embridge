<?php

/**
 * @file
 * Webform module file component.
 */

/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_entermediaasset() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'mandatory' => 0,
    'pid' => 0,
    'weight' => 0,
    'extra' => array(
      'filtering' => array(
        'types' => array('gif', 'jpg', 'png'),
        'size' => '2048',
      ),
      'asset_catalog' => '',
      'asset_path' => '',
      'scheme' => 'public',
      'directory' => '',
      'progress_indicator' => 'throbber',
      'title_display' => 0,
      'description' => '',
      'attributes' => array(),
      'private' => FALSE,
    ),
  );
}

/**
 * Implements _webform_theme_component().
 */
function _webform_theme_entermediaasset() {
  return array(
    'webform_edit_entermediaasset' => array(
      'render element' => 'form',
    ),
    'webform_display_entermediaasset' => array(
      'render element' => 'element',
    ),
  );
}

/**
 * Implements _webform_edit_component().
 */
function _webform_edit_entermediaasset($component) {
  $form = array();
  $form['#theme'] = 'webform_edit_entermediaasset';
  $form['#element_validate'] = array('embridge_component_webform_edit_entermediaasset_check_directory');
  $form['#after_build'] = array('embridge_component_webform_edit_entermediaasset_check_directory');

  $form['validation']['filtering'] = array(
    '#element_validate' => array('embridge_component_webform_edit_entermediaasset_filtering_validate'),
    '#parents' => array('extra', 'filtering'),
  );

  // Find the list of all currently valid extensions.
  $current_types = isset($component['extra']['filtering']['types']) ? $component['extra']['filtering']['types'] : array();

  $types = array('gif', 'jpg', 'png');
  $form['validation']['filtering']['types']['webimages'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Web Images'),
    '#options' => drupal_map_assoc($types),
    '#default_value' => array_intersect($current_types, $types),
  );

  $types = array('bmp', 'eps', 'tif', 'pict', 'psd');
  $form['validation']['filtering']['types']['desktopimages'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Desktop Images'),
    '#options' => drupal_map_assoc($types),
    '#default_value' => array_intersect($current_types, $types),
  );

  $types = array(
    'txt',
    'rtf',
    'html',
    'odf',
    'pdf',
    'doc',
    'docx',
    'ppt',
    'pptx',
    'xls',
    'xlsx',
    'xml',
  );

  $form['validation']['filtering']['types']['documents'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Documents'),
    '#options' => drupal_map_assoc($types),
    '#default_value' => array_intersect($current_types, $types),
  );

  $types = array('avi', 'mov', 'mp3', 'ogg', 'wav');
  $form['validation']['filtering']['types']['media'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Media'),
    '#options' => drupal_map_assoc($types),
    '#default_value' => array_intersect($current_types, $types),
  );

  $types = array('bz2', 'dmg', 'gz', 'jar', 'rar', 'sit', 'tar', 'zip');
  $form['validation']['filtering']['types']['archives'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Archives'),
    '#options' => drupal_map_assoc($types),
    '#default_value' => array_intersect($current_types, $types),
  );

  $form['validation']['filtering']['addextensions'] = array(
    '#type' => 'textfield',
    '#title' => t('Additional Extensions'),
    '#description' => t('Enter a list of additional file extensions for this upload field, separated by commas.<br /> Entered extensions will be appended to checked items above.'),
    '#size' => 60,
    '#weight' => 3,
    '#default_value' => !empty($component['extra']['filtering']['addextensions']) ? $component['extra']['filtering']['addextensions'] : '',
  );

  $form['validation']['filtering']['size'] = array(
    '#type' => 'textfield',
    '#title' => t('Max Upload Size'),
    '#default_value' => $component['extra']['filtering']['size'],
    '#description' => t('Enter the max file size a user may upload such as 2 MB or 800 KB. Your server has a max upload size of @size.', array('@size' => format_size(file_upload_max_size()))),
    '#size' => 10,
    '#weight' => 3,
    '#default_value' => $component['extra']['filtering']['size'],
    '#parents' => array('extra', 'filtering', 'size'),
    '#element_validate' => array('embridge_component_webform_edit_entermediaasset_size_validate'),
  );
  $catalogs = variable_get('embridge_catalogs', array());
  $form['extra']['asset_catalog'] = array(
    '#type' => 'select',
    '#title' => t('EnterMedia Catalog'),
    '#default_value' => !empty($component['extra']['asset_catalog']) ? $component['extra']['asset_catalog'] : EMBRIDGE_ALLOWED_ASSET_CATALOG_DEFAULT,
    '#options' => $catalogs,
    '#required' => TRUE,
    '#description' => t('Select EnterMedia catalog for asset.'),
  );
  $form['extra']['asset_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Asset Path'),
    '#default_value' => !empty($component['extra']['asset_path']) ? $component['extra']['asset_path'] : '[dam:yyyy]/[dam:mm]',
    '#required' => FALSE,
    '#description' => t('Used to create subfolders during asset upload. Use token [dam:yyyy] for current year and [dam:mm] for current month.'),
  );
  $form['display']['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#default_value' => !empty($component['extra']['width']) ? $component['extra']['width'] : '',
    '#description' => t('Width of the file field.') . ' ' . t('Leaving blank will use the default size.'),
    '#size' => 5,
    '#maxlength' => 10,
    '#weight' => 4,
    '#parents' => array('extra', 'width'),
  );
  return $form;
}

/**
 * A Form API element validate function to check filesize is numeric.
 */
function embridge_component_webform_edit_entermediaasset_size_validate($element) {
  if (!empty($element['#value'])) {
    $set_filesize = parse_size($element['#value']);
    if ($set_filesize == FALSE) {
      form_error($element, t('File size @value is not a valid filesize. Use a value such as 2 MB or 800 KB.', array('@value' => $element['#value'])));
    }
    else {
      $max_filesize = parse_size(file_upload_max_size());
      if ($max_filesize < $set_filesize) {
        form_error($element, t('An upload size of @value is too large, you are allow to upload files @max or less.', array('@value' => $element['#value'], '@max' => format_size($max_filesize))));
      }
    }
  }
}

/**
 * A Form API after build and validate function.
 *
 * Ensure that the destination directory exists and is writable.
 */
function embridge_component_webform_edit_entermediaasset_check_directory($element) {

  $scheme = !empty($element['extra']['scheme']['#value']) ? $element['extra']['scheme']['#value'] : 'public';
  $directory = !empty($element['extra']['directory']['#value']) ? $element['extra']['directory']['#value'] : '';

  $destination_dir = file_stream_wrapper_uri_normalize($scheme . '://' . $directory . '/webform');

  // TODO: It looks like it's not possible to check directory on
  // EnterMedia server.
  // In that case this function should be removed (YK).
  return $element;
}

/**
 * A Form API element validate function.
 *
 * Change the submitted values of the component so that all filtering extensions
 * are saved as a single array.
 */
function embridge_component_webform_edit_entermediaasset_filtering_validate($element, &$form_state) {
  // Predefined types.
  $extensions = array();
  foreach (element_children($element['types']) as $category) {
    foreach (array_keys($element['types'][$category]['#value']) as $extension) {
      if ($element['types'][$category][$extension]['#value']) {
        $extensions[] = $extension;
      }
    }
  }

  // Additional types.
  $additional_extensions = explode(',', $element['addextensions']['#value']);
  foreach ($additional_extensions as $extension) {
    $clean_extension = drupal_strtolower(trim($extension));
    if (!empty($clean_extension) && !in_array($clean_extension, $extensions)) {
      $extensions[] = $clean_extension;
    }
  }

  form_set_value($element['types'], $extensions, $form_state);
}

/**
 * Theming function for component settings form.
 */
function theme_webform_edit_entermediaasset($variables) {
  $form = $variables['form'];

  // Add a little JavaScript to check all the items in one type.
  $javascript = '
    function check_category_boxes() {
      var checkValue = !document.getElementById("edit-extra-filtering-types-"+arguments[0]+"-"+arguments[1]).checked;
      for(var i=1; i < arguments.length; i++) {
        document.getElementById("edit-extra-filtering-types-"+arguments[0]+"-"+arguments[i]).checked = checkValue;
      }
    }
 ';
  drupal_add_js($javascript, 'inline');

  // Format the components into a table.
  $per_row = 6;
  $rows = array();
  foreach (element_children($form['validation']['filtering']['types']) as $key => $filtergroup) {
    $row = array();
    $first_row = count($rows);
    if ($form['validation']['filtering']['types'][$filtergroup]['#type'] == 'checkboxes') {
      // Add the title.
      $row[] = $form['validation']['filtering']['types'][$filtergroup]['#title'];
      $row[] = '&nbsp;';
      // Convert the checkboxes into individual form-items.
      $checkboxes = form_process_checkboxes($form['validation']['filtering']['types'][$filtergroup]);
      // Render the checkboxes in two rows.
      $checkcount = 0;
      $jsboxes = '';
      foreach (element_children($checkboxes) as $key) {
        $checkbox = $checkboxes[$key];
        if ($checkbox['#type'] == 'checkbox') {
          $checkcount++;
          $jsboxes .= "'" . $checkbox['#return_value'] . "',";
          if ($checkcount <= $per_row) {
            $row[] = array('data' => drupal_render($checkbox));
          }
          elseif ($checkcount == $per_row + 1) {
            $rows[] = array('data' => $row, 'style' => 'border-bottom: none;');
            $row = array(array('data' => '&nbsp;'), array('data' => '&nbsp;'));
            $row[] = array('data' => drupal_render($checkbox));
          }
          else {
            $row[] = array('data' => drupal_render($checkbox));
          }
        }
      }
      // Pretty up the table a little bit.
      $current_cell = $checkcount % $per_row;
      if ($current_cell > 0) {
        $colspan = $per_row - $current_cell + 1;
        $row[$current_cell + 1]['colspan'] = $colspan;
      }
      // Add the javascript links.
      $jsboxes = drupal_substr($jsboxes, 0, drupal_strlen($jsboxes) - 1);
      $rows[] = array('data' => $row);
      $select_link = ' <a href="javascript:check_category_boxes(\'' . $filtergroup . '\',' . $jsboxes . ')">(select)</a>';
      $rows[$first_row]['data'][1] = array('data' => $select_link, 'width' => 40);
      unset($form['validation']['filtering']['types'][$filtergroup]);
    }
    elseif ($filtergroup != 'size') {
      // Add other fields to the table (ie. additional extensions).
      $row[] = $form['validation']['filtering']['types'][$filtergroup]['#title'];
      unset($form['validation']['filtering']['types'][$filtergroup]['#title']);
      $row[] = array(
        'data' => drupal_render($form['validation']['filtering']['types'][$filtergroup]),
        'colspan' => $per_row + 1,
      );
      unset($form['validation']['filtering']['types'][$filtergroup]);
      $rows[] = array('data' => $row);
    }
  }
  $header = array(
    array(
      'data' => t('Category'),
      'colspan' => '2',
    ),
    array(
      'data' => t('Types'),
      'colspan' => $per_row,
    ),
  );

  // Create the table inside the form.
  $form['validation']['filtering']['types']['table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );

  $output = drupal_render_children($form);

  return $output;
}

/**
 * Implements _webform_render_component().
 */
function _webform_render_entermediaasset($component, $value = NULL, $filter = TRUE) {
  // Cap the upload size according to the PHP limit.
  $max_filesize = parse_size(file_upload_max_size());
  $set_filesize = $component['extra']['filtering']['size'];
  if (!empty($set_filesize) && parse_size($set_filesize) < $max_filesize) {
    $max_filesize = parse_size($set_filesize);;
  }

  $element = array(
    '#type' => 'managed_file',
    '#title' => $filter ? _webform_filter_xss($component['name']) : $component['name'],
    '#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : 'before',
    '#required' => $component['mandatory'],
    '#default_value' => isset($value[0]) ? $value[0] : NULL,
    '#attributes' => $component['extra']['attributes'],
    '#element_validate' => array(
      'embridge_component_webform_validate_entermediaasset',
      'embridge_component_webform_required_entermediaasset',
    ),
    '#pre_render' => array('embridge_component_managed_file_pre_render', 'webform_file_allow_access'),
    '#upload_location' => $component['extra']['scheme'] . '://webform/' . $component['extra']['directory'],
    '#process' => array('embridge_component_managed_file_process'),
    '#progress_indicator' => $component['extra']['progress_indicator'],
    '#value_callback' => 'embridge_component_managed_file_value',
    '#description' => $filter ? _webform_filter_descriptions($component['extra']['description']) : $component['extra']['description'],
    '#weight' => $component['weight'],
    '#theme_wrappers' => array('webform_element'),
    '#webform_component' => $component,
  );

  $catalog = $component['extra']['asset_catalog'];
  $catalog = explode('/', $catalog);
  $catalog_name = array_pop($catalog);

  return $element;
}

/**
 * Render a File component.
 */
function theme_webform_render_entermediaasset($variables) {
  $element = $variables['element'];
  $component = $element['#webform_component'];
  // Add information about the existing file, if any.
  if (isset($element['#default_value'])) {
    $element['_aid']['#value'] = $element['#default_value'];
  }
  $value = $element['_aid']['#value'] ? $element['_aid']['#value'] : $element['_old']['#value'];
  $class = $component['form_key'] . '-thumb';
  $firstchild = array_shift(array_keys($element));

  if ($value && ($asset = embridge_component_get_entermediaasset($value, $component))) {
    $image = !empty($asset) ? '<img class="embridge-field-icon" alt="" title="' . $asset->filepath . '" src="' . $asset->thumbnail . '" />' : ' ';
    $element[$firstchild]['#suffix'] = '<div class="embridge-field-thumb ' . $class . '">' . l($image, $asset->preview, array('html' => TRUE)) . (isset($element['#suffix']) ? $element['#suffix'] : '') . '</div>';
    $element[$firstchild]['#description'] = '<div class="webform-newfile-message">' . t('Uploading a new file will replace the current file.') . '</div>' . (isset($element[$firstchild]['#description']) ? $element[$firstchild]['#description'] : '');
  }
  else {
    $element[$firstchild]['#suffix'] = '<div class="embridge-field-thumb ' . $class . '"></div>';
  }

  return drupal_render_children($element);
}

/**
 * A Form API element validate function.
 *
 * Ensure that the uploaded file matches the specified file types.
 */
function embridge_component_webform_validate_entermediaasset($element, &$form_state, $from_value = FALSE) {
  $component = $element['#webform_component'];
  $form_key = implode('_', $element['#parents']);

  if (empty($_FILES['files']['name'][$form_key]) || form_get_error($element)) {
    return;
  }

  // Build a human readable list of extensions:
  $extensions = $component['extra']['filtering']['types'];
  $extension_list = '';
  if (count($extensions) > 1) {
    for ($n = 0; $n < count($extensions) - 1; $n++) {
      $extension_list .= $extensions[$n] . ', ';
    }
    $extension_list .= 'or ' . $extensions[count($extensions) - 1];
  }
  elseif (!empty($extensions)) {
    $extension_list = $extensions[0];
  }

  if (in_array('jpg', $extensions)) {
    $extensions[] = 'jpeg';
  }

  $strrpos = function_exists('mb_strrpos') ? 'mb_strrpos' : 'strrpos';
  $dot = $strrpos($_FILES['files']['name'][$form_key], '.');
  $extension = drupal_strtolower(drupal_substr($_FILES['files']['name'][$form_key], $dot + 1));
  $file_error = FALSE;
  if (!empty($extensions) && !in_array($extension, $extensions)) {
    form_error($element, t("Files with the '%ext' extension are not allowed, please upload a file with a %exts extension.",
      array(
        '%ext' => $extension,
        '%exts' => $extension_list,
      )
    ));
    $file_error = TRUE;
  }

  // Now let's check the file size (limit is set in KB).
  if ($_FILES['files']['size'][$form_key] > $component['extra']['filtering']['size'] * 1024) {
    form_error($element, t("The file '%filename' is too large (%filesize KB). Please upload a file %maxsize KB or smaller.",
      array(
        '%filename' => $_FILES['files']['name'][$form_key],
        '%filesize' => (int) ($_FILES['files']['size'][$form_key] / 1024),
        '%maxsize' => $component['extra']['filtering']['size'],
      )
    ));
    $file_error = TRUE;
  }

  // Save the file to a temporary location.
  if (!$file_error && !$from_value) {
    $catalog_id = $component['extra']['asset_catalog'];
    $asset = entermediaasset_file_save_upload($element, $catalog_id, $component);
    if ($asset) {
      // Set the hidden field value.
      $parents = $element['#array_parents'];
      array_pop($parents);
      $parents[] = '_aid';
      form_set_value(array('#parents' => $parents), $asset->aid, $form_state);
    }
  }

  if ($from_value) {
    return $file_error;
  }
}

/**
 * Callback #pre_render to hide display of the upload or remove controls.
 *
 * Upload controls are hidden when a file is already uploaded. Remove controls
 * are hidden when there is no file attached. Controls are hidden here instead
 * of in file_managed_file_process(), because #access for these buttons depends
 * on the managed_file element's #value. See the documentation of form_builder()
 * for more detailed information about the relationship between #process,
 * #value, and #access.
 *
 * Because #access is set here, it affects display only and does not prevent
 * JavaScript or other untrusted code from submitting the form as though access
 * were enabled. The form processing functions for these elements should not
 * assume that the buttons can't be "clicked" just because they are not
 * displayed.
 *
 * @see file_managed_file_process()
 * @see form_builder()
 */
function embridge_component_managed_file_pre_render($element) {
  // If we already have a file, we don't want to show the upload controls.
  if (!empty($element['#value']['aid'])) {
    $element['upload']['#access'] = FALSE;
    $element['upload_button']['#access'] = FALSE;
  }
  else {
    // If we don't already have a file, there is nothing to remove.
    $element['remove_button']['#access'] = FALSE;
  }
  return $element;
}

/**
 * Implements _webform_display_component().
 */
function _webform_display_entermediaasset($component, $value, $format = 'html') {
  $aid = isset($value[0]) ? $value[0] : NULL;
  return array(
    '#title' => $component['name'],
    '#value' => $aid ? embridge_component_get_entermediaasset($aid, $component) : NULL,
    '#weight' => $component['weight'],
    '#theme' => 'webform_display_entermediaasset',
    '#theme_wrappers' => $format == 'text' ? array('webform_element_text') : array('webform_element'),
    '#webform_component' => $component,
    '#format' => $format,
  );
}

/**
 * Format the output of text data for this component.
 */
function theme_webform_display_entermediaasset($variables) {
  $element = $variables['element'];
  $asset = $element['#value'];
  $url = !empty($asset) ? $asset->preview : t('no upload');
  $image = !empty($asset) ? '<img class="embridge-field-icon" alt="" title="' . $asset->filepath . '" src="' . $asset->thumbnail . '" />' : ' ';
  $output = '<div class="embridge-field-thumb">';
  $output .= !empty($asset) ? ($element['#format'] == 'text' ? $url : l($image, $url, array('html' => TRUE))) : ' ';
  $output .= '</div>';
  return $output;
}

/**
 * Implements _webform_delete_component().
 */
function _webform_delete_entermediaasset($component, $value) {
  // We can't remove files from EnterMedia server. This function should
  // be removed.
}

/**
 * Implements _webform_analysis_component().
 */
function _webform_analysis_entermediaasset($component, $sids = array()) {
  $query = db_select('webform_submitted_data', 'wsd', array('fetch' => PDO::FETCH_ASSOC))
    ->fields('wsd', array('no', 'data'))
    ->condition('nid', $component['nid'])
    ->condition('cid', $component['cid']);

  if (count($sids)) {
    $query->condition('sid', $sids, 'IN');
  }

  $nonblanks = 0;
  $sizetotal = 0;
  $submissions = 0;

  $result = $query->execute();
  foreach ($result as $data) {
    $asset = embridge_component_get_entermediaasset($data['data'], $component);
    if (isset($asset->filesize)) {
      $nonblanks++;
      $sizetotal += $asset->filesize;
    }
    $submissions++;
  }

  $rows[0] = array(t('Left Blank'), ($submissions - $nonblanks));
  $rows[1] = array(t('User uploaded file'), $nonblanks);
  $rows[2] = array(t('Average uploaded file size'), ($sizetotal != 0 ? (int) (($sizetotal / $nonblanks) / 1024) . ' KB' : '0'));
  return $rows;
}

/**
 * Implements _webform_table_component().
 */
function _webform_table_entermediaasset($component, $value) {
  $output = '';
  $asset = embridge_component_get_entermediaasset($value[0], $component);
  if (!empty($asset->aid)) {
    $output = '<a href="' . $asset->preview . '">' . check_plain($asset->filepath) . '</a>';
    $output .= ' (' . (int) ($asset->filesize / 1024) . ' KB)';
  }
  return $output;
}

/**
 * Implements _webform_csv_headers_component().
 */
function _webform_csv_headers_entermediaasset($component, $export_options) {
  $header = array();
  // Two columns in header.
  $header[0] = array('', '');
  $header[1] = array($component['name'], '');
  $header[2] = array(t('Name'), t('Filesize (KB)'));
  return $header;
}

/**
 * Implements _webform_csv_data_component().
 */
function _webform_csv_data_entermediaasset($component, $export_options, $value) {
  $asset = embridge_component_get_entermediaasset($value[0], $component);
  return empty($asset->filename) ? array('', '') : array($asset->filepath, (int) ($asset->filesize / 1024));
}

/**
 * Helper function to load a file from the database.
 *
 * @todo We can remove this entirely now the Drupal 7 has native file loading.
 */
function embridge_component_get_entermediaasset($aid, $component) {
  $catalog_id = $component['extra']['asset_catalog'];
  $asset = NULL;
  if ($aid && $catalog_id) {
    // Get asset data from entermedia table.
    $asset = _embridge_get_asset_object($aid, $catalog_id);
    $catalogs = explode('/', $catalog_id);
    $catalog_name = array_pop($catalogs);
    if (empty($asset)) {
      $catalog_settings = _embridge_get_catalog_settings_by_name(drupal_strtolower($catalog_name));
      $xmlarr = _embridge_get_search_result(array(array('field' => 'id', 'value' => $aid)), $catalog_settings);
      $xmlarr = (array) $xmlarr;
      if (!$xmlarr['hit']) {
        watchdog('embridge_component', 'Failed to get uploaded EnterMedia asset %source - %message', array('%source' => $source_path, '%message' => $response), WATCHDOG_ERROR);
        form_set_error($source, t('Failed to get uploaded EnterMedia asset - EnterMedia service error.'));
        return FALSE;
      }

      $hit = (array) $xmlarr['hit'];
      $server_url = variable_get('embridge_server_url', '');
      $server_port = variable_get('embridge_server_port', '');

      $asset = array();
      $asset['asset_id'] = $hit['@attributes']['id'];
      $asset['title'] = isset($hit['@attributes']['assettitle']) ? $hit['@attributes']['assettitle'] : $hit['@attributes']['name'];
      $asset['filesize'] = $hit['@attributes']['filesize'];
      $asset['filemime'] = $hit['@attributes']['fileformat'];
      $asset['thumbnail'] = $server_url . ':' . $server_port . $hit['thumb'];
      $asset['preview'] = $server_url . ':' . $server_port . $hit['preview'];
      $asset['width'] = $hit['@attributes']['width'] ? $hit['@attributes']['width'] : 0;
      $asset['height'] = $hit['@attributes']['height'] ? $hit['@attributes']['height'] : 0;
      $asset['embedcode'] = isset($embedcode) ? $embedcode : '';
      $asset['sourcepath'] = $hit['@attributes']['sourcepath'];
      // Get all renditions for asset.
      $renditions = _embridge_get_rendition_list($hit['@attributes']['id'], $catalog_id, $hit['@attributes']['sourcepath'], $hit['@attributes']['fileformat'], TRUE);
      if (!empty($renditions['thumb'])) {
        $asset['thumbnail'] = $renditions['thumb'];
      }

      if (!empty($renditions['medium'])) {
        $asset['preview'] = $renditions['medium'];
      }

      // Adding entermedia asset data to entermedia table.
      $asset['aid'] = _embridge_asset_insert($asset, $catalog_id);
      $asset = (object) $asset;
    }
    $asset->filename = basename($asset->sourcepath);
    $asset->filepath = variable_get('embridge_' . drupal_strtolower($catalog_name) . '_server_mediastore', '') . '\\' . str_replace('/', '\\', $asset->sourcepath);
  }
  return $asset;
}

/**
 * Given a submission with file_usage set, add or remove file usage entries.
 */
function embridge_component_entermedia_usage_adjust($submission) {
  if (isset($submission->entermedia_usage)) {
    foreach ($submission->entermedia_usage['added_aids'] as $cid => $aids) {
      foreach ($aids as $aid) {
        embridge_component_add_value($submission->nid, $cid, $submission->sid, $aid);
      }
    }
  }
}
