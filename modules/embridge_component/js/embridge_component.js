var current_field;
(function ($) {
  Drupal.behaviors.entermedia_search = {
    attach: function() {
    $("#search_form").dialog({
      autoOpen: false,
      modal: true,
      height: 600,
      width: 800,
      draggable: false,
      resizable: false,
      zIndex: 9999,
    });
    }
  }
})(jQuery);

function entermedia_process_search_result(assets) {
  jQuery("#search_form").dialog('close');
  Drupal.attachBehaviors('.webform-client-form');
  if (assets.length > 0) {
    jQuery('.webform-client-form').find(':input[type="hidden"][name*="submitted[' + current_field + '][aid]"]').val(assets[0]['aid']);
    jQuery('.webform-client-form').find('div.' + current_field + '-thumb').html(_entermedia_create_thumb(assets[0]));
    var field_id_dashes = current_field.replace(/_/g,'-');
    jQuery('#webform-component-' + field_id_dashes).append('<input type="hidden" name="submitted[' + current_field + '][search]" value="1">');

    // Execute mousedown after we close modal frame window. This will reload field with aploaded file.
    jQuery('#edit-submitted-' + field_id_dashes + '-upload-button').mousedown();
  }
}

function entermedia_cancel_search() {
  jQuery("#search_form").dialog('close');
}

function entermedia_search(field_name, catalog_name) {
  current_field = field_name;
  jQuery("#search_form").html('<iframe id="search_iframe" width="100%" height="100%" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="auto" />').dialog('open');
  jQuery("#search_iframe").attr("src","/embridge/search/" + Drupal.settings.embridge.default_catalog + "?clear_frame=true&display_type=thumbnail");
  jQuery("#search_iframe").load(function() {
    this.style.height = this.contentWindow.document.body.offsetHeight + 50 + 'px';
  });
}

function _entermedia_create_thumb(asset) {
  var output = '<img src="' + asset['thumbnail'] + '" title="' + asset['filepath'] + '" alt="" class="embridge-field-icon">';
  return output;
}
