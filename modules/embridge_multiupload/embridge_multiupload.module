<?php

/**
 * @file
 * Module creating a multiple embridge_field upload widget.
 */

$embridge_multiupload_path = drupal_get_path('module', 'embridge_multiupload');
require_once $embridge_multiupload_path . '/embridge_multiupload.field.inc';

/**
 * Implements hook_element_info().
 */
function embridge_multiupload_element_info() {
  $embridge_field_path = drupal_get_path('module', 'embridge_field');
  $embridge_multiupload_path = drupal_get_path('module', 'embridge_multiupload');

  $types['embridge_multiupload_managed_embridge_field'] = array(
    '#input' => TRUE,
    '#process' => array('embridge_multiupload_managed_embridge_field_process'),
    '#value_callback' => 'embridge_multiupload_managed_embridge_field_value',
    '#element_validate' => array('embridge_field_managed_embridge_field_validate'),
    '#pre_render' => array('embridge_field_managed_embridge_field_pre_render'),
    '#theme' => 'embridge_field_managed_embridge_field',
    '#theme_wrappers' => array('form_element'),
    '#progress_indicator' => 'throbber',
    '#progress_message' => NULL,
    '#upload_validators' => array(),
    '#upload_location' => NULL,
    '#extended' => FALSE,
    '#size' => 22,
    '#attached' => array(
      'css' => array($embridge_field_path . '/embridge_field.css'),
      'js' => array($embridge_multiupload_path . '/embridge_multiupload.js', $embridge_field_path . '/embridge_field.js'),
    ),
  );
  return $types;
}

/**
 * Process function to expand the element type.
 *
 * Expands the embridge_field type to include Upload and Remove buttons,
 * as well as support for a default value.
 */
function embridge_multiupload_managed_embridge_field_process($element, &$form_state, &$form) {
  $element = embridge_field_managed_embridge_field_process($element, $form_state, $form);
  $element['upload']['#attributes'] = array('multiple' => 'multiple');
  $element['upload']['#name'] .= '[]';
  if (!empty($element['upload']['#attached']['js'][0]['data']['file'])) {
    $element['upload']['#attached']['js'][0]['data']['embridge_multiupload'] = $element['upload']['#attached']['js'][0]['data']['file'];
  }
  unset($element['upload']['#attached']['js'][0]['data']['file']);
  return $element;
}

/**
 * The #value_callback for a embridge_multiupload_managed_file type element.
 *
 * Mostly copied from file.module.
 */
function embridge_multiupload_managed_embridge_field_value(&$element, $input = FALSE, $form_state = NULL) {
  $aid = 0;

  $instance = field_widget_instance($element, $form_state);

  // Find the current value of this field from the form state.
  $form_state_aid = $form_state['values'];
  foreach ($element['#parents'] as $parent) {
    $form_state_aid = isset($form_state_aid[$parent]) ? $form_state_aid[$parent] : 0;
  }

  if ($element['#extended'] && isset($form_state_aid['aid'])) {
    $aid = $form_state_aid['aid'];
  }
  elseif (is_numeric($form_state_aid)) {
    $aid = $form_state_aid;
  }

  // Process any input and save new uploads.
  if ($input !== FALSE) {
    $return = $input;

    $element_parent = drupal_array_get_nested_value($form_state['complete form'], array_slice($element['#parents'], 0, -1));
    $element['#file_upload_delta_original'] = isset($element_parent['#file_upload_delta']) ? $element_parent['#file_upload_delta'] : 0;

    // Uploads take priority over all other values.
    if ($file = embridge_multiupload_managed_embridge_field_save_upload($element, $instance)) {
      $aid = $file->aid;
    }
    else {
      // Check for #filefield_value_callback values.
      // Because FAPI does not allow multiple #value_callback values like it
      // does for #element_validate and #process, this fills the missing
      // functionality to allow File fields to be extended through FAPI.
      if (isset($element['#file_value_callbacks'])) {
        foreach ($element['#file_value_callbacks'] as $callback) {
          $callback($element, $input, $form_state);
        }
      }
      // Load file if the FID has changed to confirm it exists.
      if (isset($input['aid']) && $file = embridge_field_asset_load($input['aid'])) {
        $aid = $file->aid;
      }
    }
  }

  // If there is no input, set the default value.
  else {
    if ($element['#extended']) {
      $default_aid = isset($element['#default_value']['aid']) ? $element['#default_value']['aid'] : 0;
      $return = isset($element['#default_value']) ? $element['#default_value'] : array('aid' => 0);
    }
    else {
      $default_aid = isset($element['#default_value']) ? $element['#default_value'] : 0;
      $return = array('aid' => 0);
    }

    // Confirm that the file exists when used as a default value.
    if ($default_aid && $file = embridge_field_asset_load($default_aid)) {
      $aid = $file->aid;
    }
  }

  $return['aid'] = $aid;

  return $return;
}

/**
 * Given a managed_file element, save any files that have been uploaded into it.
 *
 * Mostly copied from file.module.
 *
 * @param array $element
 *   The FAPI element whose values are being saved.
 *
 * @return object
 *   The file object representing the file that was saved, or FALSE if no file
 *   was saved.
 */
function embridge_multiupload_managed_embridge_field_save_upload($element, $instance = array()) {
  $last_parent = array_pop($element['#parents']);
  $upload_name = implode('_', $element['#parents']) . '_' . $element['#file_upload_delta_original'];
  array_push($element['#parents'], $last_parent);

  $file_number = $last_parent - $element['#file_upload_delta_original'];

  if (isset($_FILES['files']['name'][$upload_name][$file_number])) {
    $name = $_FILES['files']['name'][$upload_name][$file_number];
    if (empty($name)) {
      return FALSE;
    }

    $destination = isset($element['#upload_location']) ? $element['#upload_location'] : NULL;
    if (isset($destination) && !file_prepare_directory($destination, FILE_CREATE_DIRECTORY)) {
      watchdog('file', 'The upload directory %directory for the file field !name could not be created or is not accessible. A newly uploaded file could not be saved in this directory as a consequence, and the upload was canceled.', array('%directory' => $destination, '!name' => $element['#field_name']));
      form_set_error($upload_name, t('The file could not be uploaded.'));
      return FALSE;
    }

    if (!$file = embridge_multiupload_embridge_field_save_upload($instance, $upload_name, $file_number, $element['#upload_validators'], $destination)) {
      watchdog('file', 'The file upload failed. %upload', array('%upload' => $upload_name));
      form_set_error($upload_name, t('The file in the !name field was unable to be uploaded.', array('!name' => $element['#title'])));
      return FALSE;
    }
    return $file;
  }
  else {
    return FALSE;
  }
}

/**
 * Saves a file upload to a new location.
 *
 * Mostly copied from drupal core file /include/file.inc.
 *
 * The file will be added to the {file_managed} table as a temporary file.
 * Temporary files are periodically cleaned. To make the file a permanent file,
 * assign the status and use file_save() to save the changes.
 *
 * Rewrite of /include/file.inc
 *
 * @param array $instance
 *   Instance name.
 * @param string $source
 *   A string specifying the filepath or URI of the uploaded file to save.
 * @param array $file_number
 *   The array key of the file to save in $_FILES['files']['name'][$source].
 * @param array $validators
 *   An optional, associative array of callback functions used to validate the
 *   file. See file_validate() for a full discussion of the array format.
 *   If no extension validator is provided it will default to a limited safe
 *   list of extensions which is as follows: "jpg jpeg gif png txt
 *   doc xls pdf ppt pps odt ods odp". To allow all extensions you must
 *   explicitly set the 'file_validate_extensions' validator to an empty array
 *   (Beware: this is not safe and should only be allowed for trusted users, if
 *   at all).
 * @param string $destination
 *   A string containing the URI $source should be copied to.
 *   This must be a stream wrapper URI. If this value is omitted, Drupal's
 *   temporary files scheme will be used ("temporary://").
 * @param int $replace
 *   Replace behavior when the destination file already exists:
 *   - FILE_EXISTS_REPLACE: Replace the existing file.
 *   - FILE_EXISTS_RENAME: Append _{incrementing number} until the filename is
 *     unique.
 *   - FILE_EXISTS_ERROR: Do nothing and return FALSE.
 *
 * @return bool
 *   An object containing the file information if the upload succeeded, FALSE
 *   in the event of an error, or NULL if no file was uploaded. The
 *   documentation for the "File interface" group, which you can find under
 *   Related topics, or the header at the top of this file, documents the
 *   components of a file object. In addition to the standard components,
 *   this function adds:
 *   - source: Path to the file before it is moved.
 *   - destination: Path to the file after it is moved (same as 'uri').
 */
function embridge_multiupload_embridge_field_save_upload($instance, $source, $file_number, $validators = array(), $destination = FALSE, $replace = FILE_EXISTS_RENAME) {
  global $user;

  $catalog_id = $instance['settings']['asset_catalog'];

  // Make sure there's an upload to process.
  if (empty($_FILES['files']['name'][$source][$file_number])) {
    return NULL;
  }

  // Check for file upload errors and return FALSE if a lower level system
  // error occurred. For a complete list of errors:
  // See http://php.net/manual/en/features.file-upload.errors.php.
  switch ($_FILES['files']['error'][$source][$file_number]) {
    case UPLOAD_ERR_INI_SIZE:
    case UPLOAD_ERR_FORM_SIZE:
      drupal_set_message(t('The file %file could not be saved, because it exceeds %maxsize, the maximum allowed size for uploads.', array('%file' => $_FILES['files']['name'][$source][$file_number], '%maxsize' => format_size(file_upload_max_size()))), 'error');
      return FALSE;

    case UPLOAD_ERR_PARTIAL:
    case UPLOAD_ERR_NO_FILE:
      drupal_set_message(t('The file %file could not be saved, because the upload did not complete.', array('%file' => $_FILES['files']['name'][$source][$file_number])), 'error');
      return FALSE;

    case UPLOAD_ERR_OK:
      // Final check that this is a valid upload, if it isn't, use the
      // default error handler.
      if (is_uploaded_file($_FILES['files']['tmp_name'][$source][$file_number])) {
        break;
      }

    default:
      // Unknown error.
      drupal_set_message(t('The file %file could not be saved. An unknown error has occurred.', array('%file' => $_FILES['files']['name'][$source][$file_number])), 'error');
      return FALSE;
  }

  // Begin building file object.
  $file = new stdClass();
  $file->uid      = $user->uid;
  $file->status   = 0;
  $file->filename = trim(drupal_basename($_FILES['files']['name'][$source][$file_number]), '.');
  $file->uri      = $_FILES['files']['tmp_name'][$source][$file_number];
  $file->filemime = file_get_mimetype($file->filename);
  $file->filesize = $_FILES['files']['size'][$source][$file_number];

  $extensions = '';
  if (isset($validators['file_validate_extensions'])) {
    if (isset($validators['file_validate_extensions'][0])) {
      // Build the list of non-munged extensions if the caller provided them.
      $extensions = $validators['file_validate_extensions'][0];
    }
    else {
      // If 'file_validate_extensions' is set and the list is empty then the
      // caller wants to allow any extension. In this case we have to remove the
      // validator or else it will reject all extensions.
      unset($validators['file_validate_extensions']);
    }
  }
  else {
    // No validator was provided, so add one using the default list.
    // Build a default non-munged safe list for file_munge_filename().
    $extensions = 'jpg jpeg gif png txt doc xls pdf ppt pps odt ods odp';
    $validators['file_validate_extensions'] = array();
    $validators['file_validate_extensions'][0] = $extensions;
  }

  if (!empty($extensions)) {
    // Munge the filename to protect against possible malicious extension hiding
    // within an unknown file type (ie: filename.html.foo).
    $file->filename = file_munge_filename($file->filename, $extensions);
  }

  // Rename potentially executable files, to help prevent exploits (i.e. will
  // rename filename.php.foo and filename.php to filename.php.foo.txt and
  // filename.php.txt, respectively). Don't rename if 'allow_insecure_uploads'
  // evaluates to TRUE.
  if (!variable_get('allow_insecure_uploads', 0) && preg_match('/\.(php|pl|py|cgi|asp|js)(\.|$)/i', $file->filename) && (substr($file->filename, -4) != '.txt')) {
    $file->filemime = 'text/plain';
    $file->uri .= '.txt';
    $file->filename .= '.txt';
    // The .txt extension may not be in the allowed list of extensions. We have
    // to add it here or else the file upload will fail.
    if (!empty($extensions)) {
      $validators['file_validate_extensions'][0] .= ' txt';
      drupal_set_message(t('For security reasons, your upload has been renamed to %filename.', array('%filename' => $file->filename)));
    }
  }

  // If the destination is not provided, use the temporary directory.
  if (empty($destination)) {
    $destination = 'temporary://';
  }

  // Assert that the destination contains a valid stream.
  $destination_scheme = file_uri_scheme($destination);
  if (!$destination_scheme || !file_stream_wrapper_valid_scheme($destination_scheme)) {
    drupal_set_message(t('The file could not be uploaded, because the destination %destination is invalid.', array('%destination' => $destination)), 'error');
    return FALSE;
  }

  $file->source = $source;
  // A URI may already have a trailing slash or look like "public://".
  if (substr($destination, -1) != '/') {
    $destination .= '/';
  }
  $file->destination = file_destination($destination . $file->filename, $replace);
  // If file_destination() returns FALSE then $replace == FILE_EXISTS_ERROR and
  // there's an existing file so we need to bail.
  if ($file->destination === FALSE) {
    drupal_set_message(t('The file %source could not be uploaded because a file by that name already exists in the destination %directory.', array('%source' => $source, '%directory' => $destination)), 'error');
    return FALSE;
  }

  // Add in our check of the the file name length.
  $validators['file_validate_name_length'] = array();

  // Call the validation functions specified by this function's caller.
  $errors = file_validate($file, $validators);

  // Check for errors.
  if (!empty($errors)) {
    $message = t('The specified file %name could not be uploaded.', array('%name' => $file->filename));
    if (count($errors) > 1) {
      $message .= theme('item_list', array('items' => $errors));
    }
    else {
      $message .= ' ' . array_pop($errors);
    }
    form_set_error($source, $message);
    return FALSE;
  }

  $catalogs = variable_get('embridge_catalogs', array());
  $catalog_name = $catalogs[$catalog_id];
  $save_path = getcwd() . '/' . variable_get('file_public_path', conf_path() . '/files') . '/';
  $cust_file_name = _embridge_get_file_name($_FILES['files']['name'][$source][$file_number]);
  $fname = _embridge_get_friendly_name($cust_file_name) . '-' . round(_embridge_microtime_float());
  $file_name = $fname . '.' . _embridge_get_file_extension($_FILES['files']['name'][$source][$file_number]);

  if ($_FILES['files']['tmp_name'][$source][$file_number]) {
    $upload_file = $_FILES['files']['tmp_name'][$source][$file_number];
    if (!@move_uploaded_file($upload_file, $save_path . $file_name)) {
      if (!file_exists($upload_file)) {
        watchdog('embridge_field', 'The file %file does not exist.', array('%file' => $upload_file), WATCHDOG_ERROR);
      }
      if (!is_dir($save_path)) {
        watchdog('embridge_field', 'The directory %directory does not exist.', array('%directory' => $save_path), WATCHDOG_ERROR);
      }
      form_set_error($source, t('File upload error. Could not move uploaded file.'));
      return FALSE;
    }
  }

  $asset_path = trim($instance['settings']['asset_path']);
  $asset_path = token_replace($asset_path, array('dam' => NULL));
  $asset_directory = $asset_path;

  // Source path will be returned from method.
  $source_path = '';
  $response = _embridge_upload_file($save_path . $file_name, $asset_directory, $catalog_id, $source_path);
  $xmlobj = simplexml_load_string($response);
  $xmlarr = (array) $xmlobj;

  if (!empty($xmlarr['@attributes']['stat']) && $xmlarr['@attributes']['stat'] == 'ok') {
    $asset = (array) $xmlarr['asset'];
  }
  else {
    watchdog('embridge_field', 'Failed to upload EnterMedia asset %source - %message', array('%source' => $source_path, '%message' => $response), WATCHDOG_ERROR);
    form_set_error($source, t('Failed to upload EnterMedia asset - EnterMedia service error.'));
    return 0;
  }

  $catalog_settings = _embridge_get_catalog_settings_by_name(drupal_strtolower($catalog_name));
  $xmlarr = _embridge_get_search_result(array(array('field' => 'id', 'value' => $asset['@attributes']['id'])), $catalog_settings);
  $xmlarr = (array) $xmlarr;
  if (!$xmlarr['hit']) {
    watchdog('embridge_field', 'Failed to get uploaded EnterMedia asset %source - %message', array('%source' => $source_path, '%message' => $response), WATCHDOG_ERROR);
    form_set_error($source, t('Failed to get uploaded EnterMedia asset - EnterMedia service error.'));
    return FALSE;
  }

  $asset = (array) $xmlarr['hit'];

  $server_url = variable_get('embridge_server_url', '');
  $server_port = variable_get('embridge_server_port', '');

  $file = array();
  $file['aid'] = NULL;
  $file['asset_id'] = $asset['@attributes']['id'];
  $file['title'] = isset($asset['@attributes']['assettitle']) ? $asset['@attributes']['assettitle'] : $asset['@attributes']['name'];
  $file['filesize'] = $asset['@attributes']['filesize'];
  $file['filemime'] = $asset['@attributes']['fileformat'];
  $file['thumbnail'] = $server_url . ':' . $server_port . $asset['thumb'];
  $file['preview'] = $server_url . ':' . $server_port . $asset['preview'];
  $file['width'] = isset($asset['@attributes']['width']) ? $asset['@attributes']['width'] : 0;
  $file['height'] = isset($asset['@attributes']['height']) ? $asset['@attributes']['height'] : 0;
  $file['embedcode'] = isset($embedcode) ? $embedcode : '';
  $file['sourcepath'] = $source_path;
  // Get all renditions for asset.
  _embridge_generate_renditions($asset['@attributes']['id'], $catalog_id, $asset['@attributes']['sourcepath']);
  $renditions = _embridge_get_rendition_list($asset['@attributes']['id'], $catalog_id, $asset['@attributes']['sourcepath'], $asset['@attributes']['fileformat'], TRUE);
  if (!empty($renditions['thumb'])) {
    $file['thumbnail'] = $renditions['thumb'];
  }

  if (!empty($renditions['medium'])) {
    $file['preview'] = $renditions['medium'];
  }

  // Adding entermedia asset data to entermedia table.
  $file['aid'] = _embridge_asset_insert($file, $catalog_id);

  return (object) $file;
}

/**
 * Implements hook_insert_widgets().
 */
function embridge_multiupload_insert_widgets() {
  return array(
    'embridge_field_embridge_multiupload' => array(
      'element_type' => 'embridge_multiupload_managed_embridge_field',
      'wrapper' => '.file-widget',
      'fields' => array(
        'description' => 'input[name$="[description]"]',
      ),
    ),
  );
}
