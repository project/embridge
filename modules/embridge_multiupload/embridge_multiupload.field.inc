<?php

/**
 * @file
 * Field module functionality for the Multiple Embridge Field Widget module.
 */

/**
 * Implements hook_field_widget_info().
 */
function embridge_multiupload_field_widget_info() {
  return array(
    'embridge_field_embridge_multiupload' => array(
      'label' => t('Asset Multiupload'),
      'field types' => array('entermediaasset'),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_CUSTOM,
        'default value' => FIELD_BEHAVIOR_NONE,
      ),
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 *
 * Mostly copied from drupal core module,
 *  /modules/embridge_field/embridge_field.field.inc.
 */
function embridge_multiupload_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  // This is essentially copied from embride_field.field.inc.
  $defaults = array(
    'aid' => 0,
    'description' => '',
  );

  // Retrieve any values set in $form_state, as will be the case during AJAX
  // rebuilds of this form.
  if (isset($form_state['values'])) {
    $path = array_merge($element['#field_parents'], array($field['field_name'], $langcode));
    $path_exists = FALSE;
    $values = drupal_array_get_nested_value($form_state['values'], $path, $path_exists);
    if ($path_exists) {
      $items = $values;
      drupal_array_set_nested_value($form_state['values'], $path, NULL);
    }
  }

  foreach ($items as $i => $item) {
    $items[$i] = is_array($items[$i]) ? array_merge($defaults, $items[$i]) : $defaults;
    // Remove any items from being displayed that are not needed.
    if ($items[$i]['aid'] == 0) {
      unset($items[$i]);
    }
  }

  // Re-index deltas after removing empty items.
  $items = array_values($items);

  // Update order according to weight.
  $items = _field_sort_items($field, $items);

  // Essentially we use the embridge_multiupload_managed_embridge_field type,
  // extended with some enhancements.
  $element_info = element_info('embridge_multiupload_managed_embridge_field');
  $element += array(
    '#type' => 'embridge_multiupload_managed_embridge_field',
    '#upload_location' => embridge_field_field_widget_uri($field, $instance),
    '#upload_validators' => embridge_field_field_widget_upload_validators($field, $instance),
    '#value_callback' => 'embridge_multiupload_field_widget_value',
    '#process' => array_merge($element_info['#process'], array('embridge_multiupload_field_widget_process')),
    '#progress_indicator' => $instance['widget']['settings']['progress_indicator'],
    // Allows this field to return an array instead of a single value.
    '#extended' => TRUE,
  );

  if ($field['cardinality'] == 1) {
    // Set the default value.
    $element['#default_value'] = !empty($items) ? $items[0] : $defaults;
    // If there's only one field, return it as delta 0.
    if (empty($element['#default_value']['aid'])) {
      $element['#description'] = theme('embridge_field_upload_help', array('description' => $element['#description'], 'upload_validators' => $element['#upload_validators']));
    }
    $elements = array($element);
  }
  else {
    // If there are multiple values, add an element for each existing one.
    foreach ($items as $item) {
      $elements[$delta] = $element;
      $elements[$delta]['#default_value'] = $item;
      $elements[$delta]['#weight'] = $delta;
      $delta++;
    }
    // And then add one more empty row for new uploads except when this is a
    // programmed form as it is not necessary.
    if (($field['cardinality'] == FIELD_CARDINALITY_UNLIMITED || $delta < $field['cardinality']) && empty($form_state['programmed'])) {
      $elements[$delta] = $element;
      $elements[$delta]['#default_value'] = $defaults;
      $elements[$delta]['#weight'] = 0;
      $elements[$delta]['#required'] = ($element['#required'] && $delta == 0);
    }

    $elements['#file_upload_delta'] = $delta;
    $elements['#theme'] = 'embridge_field_widget_multiple';
    $elements['#theme_wrappers'] = array('fieldset');
    $elements['#process'] = array('embridge_multiupload_field_widget_process_multiple');
    $elements['#title'] = $element['#title'];
    $elements['#description'] = $element['#description'];
    $elements['#field_name'] = $element['#field_name'];
    $elements['#language'] = $element['#language'];

    // Add some properties that will eventually be added to the file upload
    // field. These are added here so that they may be referenced easily through
    // a hook_form_alter().
    $elements['#file_upload_title'] = t('Add a new file');
    $elements['#file_upload_description'] = theme('embridge_field_upload_help', array('description' => '', 'upload_validators' => $elements[0]['#upload_validators']));
  }

  return $elements;
}

/**
 * Implements hook_field_widget_settings_form().
 */
function embridge_multiupload_field_widget_settings_form($field, $instance) {
  $form = embridge_field_field_widget_settings_form($field, $instance);
  $form['#attached']['js'] = array(drupal_get_path('module', 'multiupload_embridge_field_widget') . '/embridge_multiupload.js');
  return $form;
}

/**
 * Get the upload validators for a file field.
 *
 * @param array $field
 *   A field array.
 * @param string $instance
 *   An instance.
 *
 * @return array
 *   An array suitable for passing to file_save_upload() or the file field
 *   element's '#upload_validators' property.
 */
function embridge_multiupload_field_widget_upload_validators($field, $instance) {
  return embridge_field_field_widget_upload_validators($field, $instance);
}

/**
 * The #value_callback for embridge_field_embridge_multiupload field element.
 */
function embridge_multiupload_field_widget_value($element, $input = FALSE, &$form_state) {
  if ($input) {
    // Checkboxes lose their value when empty.
    // If the display field is present make sure its unchecked value is saved.
    $field = field_widget_field($element, $form_state);
  }

  // Depend on embridge_multiupload managed embridge_field element for uploads.
  $return = embridge_multiupload_managed_embridge_field_value($element, $input, $form_state);

  // Ensure that all the required properties are returned even if empty.
  $return += array(
    'aid' => 0,
    'description' => '',
  );

  $last_parent = $element['#parents'][count($element['#parents']) - 1];
  $form_state['values'][$element['#field_name']]['und'][$last_parent] = $return;

  return $return;
}

/**
 * Process callback for the embridge_multiupload_embridge_field field type.
 *
 * Expands the embridge_multiupload_field type to include
 * the description and display fields.
 */
function embridge_multiupload_field_widget_process($element, &$form_state, &$form) {
  return embridge_field_field_widget_process($element, $form_state, $form);
}

/**
 * Process callback for a group of embridge_multiupload_embridge_field fields.
 *
 * Mostly copied from drupal core module
 * /module/embridge_field/embridge_field.field.inc.
 *
 * Adds the weight field to each row so it can be ordered and adds a new AJAX
 * wrapper around the entire group so it can be replaced all at once.
 */
function embridge_multiupload_field_widget_process_multiple($element, &$form_state, $form) {
  $upload_name = implode('_', $element['#parents']) . '_' . $element['#file_upload_delta'];
  if (isset($_FILES['files']['name']) && array_key_exists($upload_name, $_FILES['files']['name'])) {
    $count = count($_FILES['files']['name'][$upload_name]);
    // Supposing #file_upload_delta is always the last delta this will work.
    for ($i = 1; $i < $count; $i++) {
      $element[] = $element[$element['#file_upload_delta']];
    }
  }
  $element_children = element_children($element);
  $count = count($element_children);

  foreach ($element_children as $delta => $key) {
    if ($key < $element['#file_upload_delta']) {
      $description = embridge_field_field_get_description_from_element($element[$key]);
      $element[$key]['_weight'] = array(
        '#type' => 'weight',
        '#title' => $description ? t('Weight for @title', array('@title' => $description)) : t('Weight for new file'),
        '#title_display' => 'invisible',
        '#delta' => $count,
        '#default_value' => $delta,
      );
    }
    else {
      // The title needs to be assigned to the upload field so that validation
      // errors include the correct widget label.
      $element[$key]['#title'] = $element['#title'];
      $element[$key]['_weight'] = array(
        '#type' => 'hidden',
        '#default_value' => $delta,
      );
    }
  }

  // Add a new wrapper around all the elements for AJAX replacement.
  $element['#prefix'] = '<div id="' . $element['#id'] . '-ajax-wrapper">';
  $element['#suffix'] = '</div>';

  return $element;
}

/**
 * Submit handler for embridge_field_embridge_multiupload fields.
 *
 * For Upload/Remove buttons, this runs in addition to and after
 * embridge_field_managed_embridge_field_submit().
 *
 * @see embridge_field_managed_embridge_field_submit()
 * @see embridge_field_field_widget_form()
 * @see embridge_field_field_widget_process()
 */
function embridge_multiupload_field_widget_submit($form, &$form_state) {
  return embridge_field_field_widget_submit($form, $form_state);
}
